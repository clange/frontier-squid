FROM cern/cc7-base

LABEL maintainer="Clemens Lange <clemens.lange@cern.ch>"

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="Frontier squid cache server" \
      org.label-schema.description="A Frontier squid cache server for use with CernVM-FS" \
      org.label-schema.url="http://gitlab.cern.ch/clange/paper-containerd" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url=$VCS_URL \
      org.label-schema.vendor="CERN" \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"

ENV DUMB_INIT_VERSION=${DUMB_INIT:-1.2.2}
ENV SQUID_CACHE_DIR=/var/spool/squid
ENV SQUID_LOG_DIR=/var/log/squid
ENV SQUID_USER=squid

ENV SQUID_LISTEN_PORT=${SQUID_LISTEN_PORT:-3128}
ENV SQUID_MAX_CACHE_SIZE=${SQUID_MAX_CACHE_SIZE:-5000}
ENV SQUID_MAX_CACHE_OBJECT=${SQUID_MAX_CACHE_OBJECT:-1024}

ENV PYTHONUNBUFFERED=1

EXPOSE ${SQUID_LISTEN_PORT}

# Perform the installation as root
USER root
WORKDIR /tmp

# Create squid user and group
RUN groupadd -g 1000 ${SQUID_USER} && adduser -u 1000 -g 1000 -G root ${SQUID_USER} && \
    echo "${SQUID_USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers

RUN rpm -Uvh http://frontier.cern.ch/dist/rpms/RPMS/noarch/frontier-release-1.1-1.noarch.rpm && \
    yum install -y frontier-squid sudo iptables initscripts && \
    yum clean all

RUN curl -OL https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 && \
    mv dumb-init_${DUMB_INIT_VERSION}_amd64 /bin/dumb-init

# ADD patch_customize.py /bin/patch_customize.py
ADD customize.sh /etc/squid/customize.sh
ADD entrypoint.sh /home/${SQUID_USER}/entrypoint.sh

RUN chmod a+x /bin/dumb-init && \
    chmod a+x /home/${SQUID_USER}/entrypoint.sh && \
    mkdir -p ${SQUID_LOG_DIR} && \
    chmod -R a+w ${SQUID_LOG_DIR} && \
    chown -R ${SQUID_USER}:${SQUID_USER} ${SQUID_LOG_DIR} && \
    mkdir -p ${SQUID_CACHE_DIR} && \
    chmod -R a+w ${SQUID_CACHE_DIR} && \
    chown -R ${SQUID_USER}:${SQUID_USER} ${SQUID_CACHE_DIR} && \
    chkconfig frontier-squid on

USER ${SQUID_USER}
ENV USER ${SQUID_USER}
WORKDIR /home/${SQUID_USER}

ENTRYPOINT ["sudo", "/bin/dumb-init"]
CMD [ "/home/squid/entrypoint.sh" ]