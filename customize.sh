#!/bin/bash
#
# Edit customize.sh as you wish to customize squid.conf.
# It will not be overwritten by upgrades.
# See customhelps.awk for information on predefined edit functions.
# In order to test changes to this, run this to regenerate squid.conf:
#	service frontier-squid
# and to reload the changes into a running squid use
#	service frontier-squid reload
# Avoid single quotes in the awk source or you have to protect them from bash.
#

awk --file `dirname $0`/customhelps.awk --source '{
setoption("acl NET_LOCAL src", "10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 fc00::/7 fe80::/10 188.184.103.18 188.185.91.99 137.138.151.160 137.138.151.125")
setoption("minimum_expiry_time", "0")
setoption("maximum_object_size", "1024 MB")
setoption("cache_mem", "128 MB")
setoption("maximum_object_size_in_memory", "128 KB")
setoptionparameter("cache_dir", 3, "20000")
print
}'
