# Frontier squid cache server

General instructions can be found on the
[Frontier InstallSquid Twiki](https://twiki.cern.ch/twiki/bin/view/Frontier/InstallSquid)

## Getting started

On a local computer, run:

```shell
docker run --rm -it -p 3128:3128 --cap-add=NET_ADMIN frontier-squid
```

The `NET_ADMIN` flags are needed for `iptables` to work.

Alternatively, with `podman`:

```shell
podman run --name frontier-squid -dt -p 3128:3128/tcp gitlab-registry.cern.ch/clange/paper-containerd/frontier-squid
```

Check [sameersbn/docker-squid](https://github.com/sameersbn/docker-squid/tree/master/kubernetes) might be useful for deploying with Kubernetes at a later stage.

## Configuration

Settings are based on the
[CernVM-FS Squid Proxy instructions](https://cvmfs.readthedocs.io/en/stable/cpt-squid.html)
and implemented in [customize.sh](customize.sh)

### Sending logs to stdout

```config
logfile_rotate 0
cache_log stdio:/dev/stdout
access_log stdio:/dev/stdout
cache_store_log stdio:/dev/stdout
```

## Testing

From a machine that is supposed to use the cache proxy:

```shell
# Confirm connectivity is OK
python3 fnget.py --url=http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier --sql="select 1 from dual"
# Set proxy
export http_proxy=http://paper-containerd-squid.cern.ch:3128
# Try again
python3 fnget.py --url=http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier --sql="select 1 from dual"
```

In the frontier-squid container one should be able to see the connections:

```shell
watch -n 5 "podman logs frontier-squid |tail"
```

Output:

```output
137.138.150.36 - - [14/Oct/2020:18:35:52.620 +0200] "GET http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier/type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RMFRIK8rPVUgpTcwBAD0rB
mw_ HTTP/1.1" 403 4343 TCP_DENIED:HIER_NONE 2 "fnget.py 1.9 -" "-" "Python-urllib/2.7"
137.138.151.160 - - [14/Oct/2020:18:38:19.600 +0200] "GET http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier/type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RMFRIK8rPVUgpTcwBAD0r
Bmw_ HTTP/1.1" 200 823 TCP_MISS:HIER_DIRECT 5 "fnget.py 1.9 -" "-" "Python-urllib/3.8"
137.138.151.160 - - [14/Oct/2020:18:38:21.431 +0200] "GET http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier/type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RMFRIK8rPVUgpTcwBAD0r
Bmw_ HTTP/1.1" 200 822 TCP_MEM_HIT:HIER_NONE 0 "fnget.py 1.9 -" "-" "Python-urllib/3.8"
137.138.151.125 - - [14/Oct/2020:18:38:58.315 +0200] "GET http://cmsfrontier.cern.ch:8000/FrontierProd/Frontier/type=frontier_request:1:DEFAULT&encoding=BLOBzip&p1=eNorTs1JTS5RMFRIK8rPVUgpTcwBAD0r
Bmw_ HTTP/1.1" 200 822 TCP_MEM_HIT:HIER_NONE 0 "fnget.py 1.9 -" "-" "Python-urllib/3.8"
```

- `TCP_DENIED`: for machines that should not have access
- `TCP_MISS`: for new requests
- `TCP_MEM_HIT`: for requests that have a cache hit
