#!/bin/bash
echo
echo '    ______                 __  _                               _     __'
echo '   / ____/________  ____  / /_(_)__  _____   _________ ___  __(_)___/ /'
echo '  / /_  / ___/ __ \/ __ \/ __/ / _ \/ ___/  / ___/ __ `/ / / / / __  / '
echo ' / __/ / /  / /_/ / / / / /_/ /  __/ /     (__  ) /_/ / /_/ / / /_/ /  '
echo '/_/   /_/   \____/_/ /_/\__/_/\___/_/     /____/\__, /\__,_/_/\__,_/   '
echo '                                                  /_/                  '
echo "                  $(squid --version | head -n 1)"
echo
# Make sufficient file descriptors available
ulimit -n 16384

echo "#####################################"
echo "#      Patching customize.sh        #"
echo "#####################################"
# python /bin/patch_customize.py

echo "#####################################"
echo "#  Starting frontier-squid service  #"
echo "#####################################"
service frontier-squid start

echo "#####################################"
echo "#       Dumping squid config        #"
echo "#####################################"
squid -k parse

echo "#####################################"
echo "# Tailing /var/log/squid/access.log #"
echo "#####################################"
tail -f /var/log/squid/access.log
